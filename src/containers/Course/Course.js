import React, { Component } from 'react';

class Course extends Component {
    state = {
        id: '',
        title: '',
    }
    componentDidMount() {
        this.loadData()
    }
    componentDidUpdate() {
        this.loadData();
    }

    loadData() {
        const { id } = this.props.match.params;
        if (this.state.id !== id) {
            const { search } = this.props.location;
            const query = new URLSearchParams(search.slice(1));
            let title = '';
            if (query.has('title')) {
                title = query.get('title');
            }
            this.setState({
                id,
                title,
            })
        }
    }
    render () {
        const { id, title } = this.state;
        return (
            <div>
                <h1>{title}</h1>
                <p>You selected the Course with ID: {id}</p>
            </div>
        );
    }
}

export default Course;