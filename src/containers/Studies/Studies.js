import React, { Component } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';

import Courses from '../Courses/Courses';
import Users from '../Users/Users';
import NotFound from '../NotFound/NotFound';

import './Studies.css';

class Studies extends Component {
  render() {
    return (
      <div className="Studies">
        <ul>
          <li><a href="/">Home</a></li> 
          <li><a href="/courses">Courses</a></li>
          <li><a href="/users">Users</a></li> 
        </ul>
        <Switch>
          <Route path="/" exact render={() => <h1>Home page</h1>} />
          <Route path="/courses" component={Courses} />
          <Route path="/users" exact component={Users} />
          <Redirect from='/all-courses' to="/courses" />
          <Route path="/" component={NotFound} />
        </Switch>
      </div>
    );
  }
}

export default Studies;